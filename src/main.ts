import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle('Business App')
    .setDescription(process.env.DOC_DESCRIPTION)
    .setVersion('1.0')
    .addTag('User')
    .addTag('Company')
    .addTag('Helpers')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  await app.listen(process.env.NODE_ENV == 'development' ? 3000 :  process.env.PORT);
}
bootstrap();
