import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { UserService } from '../services/user.service';

@Injectable()
export class AuthService {

    private Bcrypt = bcrypt;

    constructor(private userService: UserService,
                private jwtService: JwtService){}

    async validateUser(email: string, pass: string): Promise<any> {
        const user: any = await this.userService.getByemail(email, true);
        if(!user) return null;
        console.log(pass, user.password)
        if(!this.Bcrypt.compareSync(pass, user.password)) return null;
        return user;
    }

    async login({email, password}): Promise<any> {
        const user = await this.validateUser(email, password);
        if(user){
            const { profile, email, companyId, role,  _id } = user
            const access_token = this.jwtService.sign({ profile, email, role, _id  })  
            return {user:{ profile, email, companyId, role,  _id  }, access_token}
        }
        return null
    }

}
