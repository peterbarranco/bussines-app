import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, BadRequestException, Param, UseGuards, Req } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiQuery,
    ApiResponse,
    ApiTags,
  } from '@nestjs/swagger';
import { createUserDTO, editUserDTO } from './dto/user.dto';
import { UserService } from "../services/user.service";
import { User } from './interfaces/user.interface';
import { user, Report, Genero, addCompany } from './entities/user.entity';
import { company } from 'faker';


@ApiBearerAuth()
@ApiTags('User')
@Controller('user')
export class UserController {


    constructor(private userService: UserService){}

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Listar todos los usuario'  })
    @ApiResponse({ status: 200, description: 'Responde un array de usuarios', type: [user] })
    @Get('/')
    async getUsers(@Res() res: any): Promise<user[]>{
        const users = await this.userService.getUsers(); 
        return res.status(HttpStatus.OK).json({users})
    }
    
    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene user por id' })
    @ApiResponse({ status: 200, description: 'Responde un objeto usuario', type: user })
    @Get('/:userId')
    async get(@Res() res: any, @Param('userId') userId: string ): Promise<user>{
        try {
            const user = await this.userService.get(userId); 
            return res.status(HttpStatus.OK).json({
                ok: true,
                data: user
            })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene user por id' })
    @ApiResponse({ status: 200, description: 'Responde un objeto usuario', type: user })
    @Get('/byEmail/:email')
    async getByEmail(@Res() res: any, @Param('email') email: string ): Promise<User>{
        console.log(email)
        try {
            const user = await this.userService.getByemail(email); 
            return res.status(HttpStatus.OK).json({user})
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene user por id de la compañia' })
    @ApiResponse({ status: 200, description: 'Responde array de objetos usuario', type: [user]})
    @Get('/company/:companyId')
    async getByCompany(@Res() res: any, @Param('companyId') company: string ): Promise<User[]>{
        try {
            const user = await this.userService.getByCompany(company); 
            return res.status(HttpStatus.OK).json({
                ok: true,
                data: user
            })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene el reporte de la cantidad usuarios por edad y genero'})
    @ApiResponse({ status: 200, description: 'Responde un array con la cantidad de usuarios agrupados por edad', type: [Report] })
    @ApiQuery({ name: 'gender', enum: Genero, required: false })
    @ApiQuery({ name: 'age', type: Number, required: false })
    @Get('/report/query')
    async getReport(@Res() res: any, @Req() req){
        try {
            const users = await this.userService.getReport({age: req?.query?.age || null, gender: req.query.gender ||  null}); 
            return res.status(HttpStatus.OK).json({
                ok: true,
                data: users
            })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }



    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Agrega una compañia a un usuario'})
    @ApiResponse({ status: 200, description: 'Responde un objeto usuario actualizado', type: user })
    @Post('/addCompany')
    async addCompany(@Res() res: any, @Body() body: addCompany): Promise<User>{
        let { companyId, _id } = body
        try {
            const user = await this.userService.addCompany({ companyId, _id }); 
            return res.status(HttpStatus.OK).json({user})
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }


    // @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Registro de usuario'})
    @ApiResponse({ status: 200, description: 'Responde un objeto usuario', type: user })
    @Post()
    async create(@Res() res: any, @Body() createUserDTO: createUserDTO): Promise<User>{
        const exists = await this.userService.getByemail(createUserDTO.email)
        if(exists) throw new BadRequestException('User already exists')
        try {
            const user = await this.userService.create(createUserDTO)
            if(user){
                return res.status(HttpStatus.OK).json({
                    ok: true,
                    data: user,
                    message:'User created successfully' 
                })
            }
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Actualiza un usuario'})
    @ApiResponse({ status: 200, description: 'Responde un objeto usuario', type: user })
    @Put('/:userId')
    async edit(@Res() res: any, @Body() payload: editUserDTO, @Param('userId') userId: string ){
        const user = await this.userService.get(userId)
        if(!user) throw new BadRequestException('User not exists')
        console.log(user)
        if(user && payload.email && payload.email !== user.email){
            const emailExists = await this.userService.getByemail(payload.email)
            if(emailExists) throw new BadRequestException('Email already Exists, please use other')
        }
        try {
            payload._id = userId 
            const result = await this.userService.edit(payload)
            return res.status(HttpStatus.OK).json({ result })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Elimina un usuario'})
    @ApiResponse({ status: 200, description: 'Devuelve un true si elimino y false en caso contrario', type: Boolean })
    @Delete('/:userId')
    async delete(@Res() res: any, @Param('userId') userId: string ){
        try {
            const result = await this.userService.deleteOne(userId)
            return res.status(HttpStatus.OK).json({ ok: true, message: 'User deleted successfully' })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

}
