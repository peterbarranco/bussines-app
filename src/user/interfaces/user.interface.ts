import { Document, Types } from "mongoose";

export class User extends Document {
    email:      string;
    password:   string;
    companyId:  Types.ObjectId;
    profile:  {
        name:     string;
        lastName: string;
        age:      number;
        gender:   string;
        documentNumber: string;
        role:     string;
    }
}