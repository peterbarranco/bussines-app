import { Types } from "mongoose";
import { ApiProperty } from '@nestjs/swagger';

export class createUserDTO {
    @ApiProperty({ example: 'mail@mail.com', description: 'email' })
    email:      string;
    @ApiProperty({ example: '123456', description: 'Password' })
    password:   string;
    @ApiProperty({ example: "5fbb6dc38a152a59cf8ff649", description: 'ObjectId de la Compañia' })
    companyId:  Types.ObjectId;
    @ApiProperty({ example: "{}", description: 'Datos personales del usuario', type: () => Profile })
    profile: Object
}

class Profile{
    @ApiProperty({ example: "juan", description: 'Nombre usuario'})
    name: string;
    @ApiProperty({ example: "barrios", description: 'Apellido usuario'})
    lastname: string;
    @ApiProperty({ example: "18", description: 'Edad'})
    age: string;
    @ApiProperty({ example: "M", description: 'Genero'})
    gender: string;
    @ApiProperty({ example: "123435", description: 'Numero de identificacion'})
    documentNumber: string;
    @ApiProperty({ example: "user", description: 'Rol del ususario'})
    role: string;
}

export class editUserDTO {
    _id:            string;
    email:          string;
    password:       string;
    companyId:      Types.ObjectId;
    profile:  {
        name:       string;
        lastName:   string;
        age:        number;
        gender:     string;
        documentNumber: string;
        role:       string;
    }
}

export class AddCompanyUserDTO {
    companyId:      Types.ObjectId;
    _id:            Types.ObjectId;
    
}