import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";

import { UserController } from './user.controller';
import { UserModel } from "./models/user.model";
import { UserService } from '../services/user.service';
import { ServicesModule } from 'src/services/services.module';

@Module({
  imports:[
    ServicesModule,
    MongooseModule.forFeature([{name: 'User', schema: UserModel}])
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
