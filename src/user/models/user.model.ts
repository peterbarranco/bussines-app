import {Schema, Types } from 'mongoose'


export const UserModel = new Schema({
    email:    {type: String, required: [true, 'Email is required'], unique:true},
    password: {type: String, required: [true, 'Password is required']},
    companyId:{type: Types.ObjectId, default: null},
    profile:  {
        name:     {type: String, required: [true, 'Name is required']},
        lastName: {type: String, required: [true, 'Lastname is required']},
        age:      {type: Number, required: [true, 'age is required']},
        gender:   {type: String, enum:['M','F'], required: [true, 'Gender is required'] },
        documentNumber: {type: String, required: [true, 'Document number is required']},
        role:     {type: String, enum:['user','admin'], required: [true, 'Role is required'], default: 'user'}
    },
}, { timestamps: true, versionKey:false })