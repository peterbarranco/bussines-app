import { ApiExtraModels, ApiProperty } from '@nestjs/swagger';
import { Types } from "mongoose";
import { type } from 'os';

export class user {
    @ApiProperty({ example: '5fbb6dc38a152a59cf8ff649', description: 'ObjectId del usuario' })
    _id: Types.ObjectId;
    @ApiProperty({ example: 'mail@mail.com', description: 'email' })
    email:      string;
    password:   string;
    @ApiProperty({ example: "5fbb6dc38a152a59cf8ff649", description: 'ObjectId de la Compañia' })
    companyId:  Types.ObjectId;
    @ApiProperty({ example: "{}", description: 'Datos personales del usuario', type: () => Profile })
    profile: Object
}

class Profile{
    @ApiProperty({ example: "juan", description: 'Nombre usuario'})
    name: string;
    @ApiProperty({ example: "barrios", description: 'Apellido usuario'})
    lastname: string;
    @ApiProperty({ example: "18", description: 'Edad'})
    age: string;
    @ApiProperty({ example: "M", description: 'Genero'})
    gender: string;
    @ApiProperty({ example: "123435", description: 'Numero de identificacion'})
    documentNumber: string;
    @ApiProperty({ example: "user", description: 'Rol del ususario'})
    role: string;
}

export class Report{
    @ApiProperty({ example: "10", description: 'Cantidad de usuarios'})
    cantidad: string;
    @ApiProperty({ example: "19", description: 'Edad'})
    edad: string;
    @ApiProperty({ enum:['M','F'], description: 'Genero del ususario'})
    genero: Genero;
}

export enum Genero {
    M = 'M',
    F = 'F'
  }

export class login{
    @ApiProperty({ example: "mail@mail.com", description: 'email de usuario'})
    email: string;
    @ApiProperty({ example: "2434564", description: 'Password'})
    password: string;
}

export class addCompany{
    @ApiProperty({ example: "5fbb6dc38a152a59cf8ff649", description: 'Id de la company'})
    companyId: Types.ObjectId;;
    @ApiProperty({ example: "5fbb6dc38a152a59cf8ff649", description: 'Id del usuario'})
    _id: Types.ObjectId;;
}
