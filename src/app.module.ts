import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ServicesModule } from './services/services.module';
import { UserModule } from './user/user.module';
import { CompanyModule } from './company/company.module';
import { CommandModule } from 'nestjs-command';
import { ConfigModule } from '@nestjs/config';
@Module({
  imports: [
    ConfigModule.forRoot({envFilePath: [__dirname + '/../enve/development.env', __dirname + '/../env/production.env'], isGlobal: true,}),
    MongooseModule.forRoot(
      process.env.DB_URI,
      { useCreateIndex: true }
    ),
    AuthModule,
    UserModule,
    ServicesModule,
    CompanyModule,
    CommandModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  
}
