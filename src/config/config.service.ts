const fs = require('fs');
import { parse } from 'dotenv';

export class ConfigService {
  private readonly envConfig: { [key: string]: string };
  
  constructor() {
    const isDevelopmentEnv = process.env.NODE_ENV !== 'production';
    let envFilePath: string;
    console.log(process.env.NODE_ENV , isDevelopmentEnv)
    if (isDevelopmentEnv) {
      envFilePath = __dirname + '/../../env/development.env';
    } else {
      envFilePath = __dirname + '/../../env/production.env';
    }
    const existsPath = fs.existsSync(envFilePath);

    if (!existsPath) {
      console.log('.env file does not exists');
      process.exit(0);
    }

    this.envConfig = parse(fs.readFileSync(envFilePath));
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}
