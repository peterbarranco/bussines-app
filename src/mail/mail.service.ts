import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}

  /**
   * @param template template name in directory /templates
   * @param subject subject 
   * @param data object data for template
   * @param emails array string of email
   * @author jgonzalez
   */
  async sendEmail(template: string, subject: string, data: object, emails: Array<string> ): Promise<any> {
    return await this.mailerService.sendMail({
      to: emails.toString(), 
      from: 'no-responder@ascensoresmover.com.co', 
      subject,
      template,
      context: data
    })
    // .then(() => {
    //   console.log('se envio correo')
    // })
    // .catch(error => console.log({error}));
  }
}
