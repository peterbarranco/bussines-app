import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CommandModule } from 'nestjs-command';
import { UploadService } from './upload.service';
import { UserService } from './user.service';
import { UserModel } from '../user/models/user.model';
import { SeedService } from './seed.service';
import { CompanyService } from './company.service';
import { CompanyModel } from '../company/models/company.model';

@Module({
    imports:[
        MongooseModule.forFeature([{name: 'User', schema: UserModel}, {name: 'Company', schema: CompanyModel}])
    ],
    providers: [UploadService,  UserService, SeedService, CompanyService],
    exports:[UploadService, UserService, SeedService, CompanyService]
})
export class ServicesModule {}
