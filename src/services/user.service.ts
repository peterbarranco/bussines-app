import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model, Types } from "mongoose";
import * as bcrypt from 'bcrypt';
import { User } from "../user/interfaces/user.interface";
import { createUserDTO, editUserDTO, AddCompanyUserDTO } from '../user/dto/user.dto';
import { AnyARecord } from 'dns';
import passport from 'passport';

@Injectable()
export class UserService {

    private Bcrypt = bcrypt;

    constructor(@InjectModel('User') private userModel: Model<User>){}

    async getUsers(): Promise<User[]>{
        const users = await this.userModel.find({},{ password: 0 });
        return users 
     }

    async getByemail(email: string, login: Boolean = false): Promise<User>{
        console.log(email)
        const password: number = login ? 1 : 0
        const user = await this.userModel.findOne({email});
        return user 
    }

    async get(userId: string): Promise<User>{
        const user = await this.userModel.findById(userId);
        return user 
    }

    async getByCompany(company: string): Promise<User[]>{
        return await this.userModel.find({companyId: Types.ObjectId(company)});
    }

    async getReport(query): Promise<any>{
        console.log(query)
        let {age,gender} = query
        let match = {}
        
        const group = { _id: "$profile.age",cantidad: { $sum: 1 },genero: {"$first": "$profile.gender"}}
        if(age && gender){
            match = { $and: [
                { 'profile.gender': { $regex: gender, $options: 'i' } },
                { 'profile.age': Number(age) },
            ]}
        }else{
            match = {'profile.age': {$gte: 19, $lte: 23}} 
        }
        const pipeline = [
            {$match: match},
            {$group: group},
            {$addFields: {edad: "$_id"}},
            { $project: { _id: 0, edad: 1, cantidad: { $toInt: "$cantidad" }, genero:1} },
            {$sort: {edad: 1}}
        ]
        return await this.userModel.aggregate(pipeline)

    }

    async create(createUserDTO): Promise<User>{
        const salt = await this.Bcrypt.genSalt(10)
        createUserDTO.password = this.Bcrypt.hashSync(createUserDTO.password, salt)
        createUserDTO.role = createUserDTO.role || 'user'
        return await this.userModel.create(createUserDTO) 
    }

    async edit(user: editUserDTO): Promise<User>{
        const { _id } = user;
        delete user._id
        const salt = await this.Bcrypt.genSalt(10)
        if(user.password){
            user.password = this.Bcrypt.hashSync(user.password, salt)
        }
        return await this.userModel.findOneAndUpdate({_id}, user, {new: true})
        
    }

    async deleteOne(userId: string): Promise<void>{
        await this.userModel.deleteOne({_id: Types.ObjectId(userId)})
    }

    async deleteAll(): Promise<void>{
        await this.userModel.deleteMany({})
    }


    async addCompany(addCompanyUserDTO: AddCompanyUserDTO): Promise<User>{
        const { _id } = addCompanyUserDTO;
        delete addCompanyUserDTO._id
        return await this.userModel.findOneAndUpdate({_id}, {companyId: addCompanyUserDTO.companyId}, {new: true})
    }


    

}
