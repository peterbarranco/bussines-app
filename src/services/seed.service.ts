import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import * as faker from 'faker'
import { UserService } from './user.service';
import { CompanyService } from './company.service';


@Injectable()
export class SeedService {

    min = 19;
    max = 23;
    totalCompanies = 3
    totalUsers = 20
    gender: String[] = ['M','F']

    constructor(private userService: UserService, private companyService:CompanyService){}

    async createSeed(seedCreate){
        let {companies, users} = seedCreate
        let admin: any

        //? clean database
        await this.userService.deleteAll()
        await this.companyService.deleteAll()
        
        //? set default values if query request is empty
        companies = companies || this.totalCompanies
        users = users || this.totalUsers

        for (let i = 0; i < companies; i++) {
            //? create company
            const companycreated = await this.companyService.create({
                name: faker.company.companyName(),
                nit: faker.random.number(),
                address: faker.address.streetAddress()
            })
            
            let user = {
                email: faker.internet.email() ,
                password: faker.internet.password() ,
                companyId: null,
                profile:  {
                    name:     faker.name.firstName(),
                    lastName: faker.name.lastName(),
                    age:      Math.floor(Math.random() * (this.max - this.min + 1)) + this.min,
                    gender:   this.gender[Math.floor(Math.random() * this.gender.length)],
                    documentNumber: faker.random.number(),
                    role: 'admin'
                }
            }
            
            //? create user admin
            if(i == 0){
                const password = user.password
                admin = await this.userService.create(user)
                admin._doc.password = password
            }

            for (let j = 0; j < users; j++) {
                user.email = faker.internet.email() 
                user.password = faker.internet.password() 
                user.companyId = Types.ObjectId(companycreated._id),
                user.profile =  {
                    name:     faker.name.firstName(),
                    lastName: faker.name.lastName(),
                    age:      Math.floor(Math.random() * (this.max - this.min + 1)) + this.min,
                    gender:   this.gender[Math.floor(Math.random() * this.gender.length)],
                    documentNumber: faker.random.number(),
                    role: 'user'
                }
                
                //? create user
                await this.userService.create(user)
            }
        }

        //? return user admin
        
        return admin._doc
    }

}


