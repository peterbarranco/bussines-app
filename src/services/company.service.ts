import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CompanyModel } from '../company/models/company.model';
import { Company } from '../company/interfaces/company.interface';
import { editCompanyDTO } from '../company/dto/company.dto';

@Injectable()
export class CompanyService {

    constructor(@InjectModel('Company') private companyModel: Model<Company>){}

    async getCompanies(): Promise<Company[]>{
        return await this.companyModel.find({},{ password: 0 });
     }

    async get(companyId: string): Promise<Company>{
        const user = await this.companyModel.findById(companyId);
        return user 
    }

    async create(createCompanyDTO): Promise<Company>{
        return await this.companyModel.create(createCompanyDTO) 
    }

    async edit(company: editCompanyDTO): Promise<Company>{
        const { _id } = company;
        delete company._id
        return await this.companyModel.findOneAndUpdate({_id}, company, {new: true})
        
    }

    async deleteOne(companyId: string): Promise<void>{
        await this.companyModel.deleteOne({_id: Types.ObjectId(companyId)})
    }

    async deleteAll(): Promise<void>{
        await this.companyModel.deleteMany({})
    }

    async getByNit(nit: number): Promise<Company>{
        return await this.companyModel.findOne({nit})
    }

    async byTerm(term: String, pag: any): Promise<Company[]>{
        let skip = Number(pag.skip) || 0;
        let limit = Number(pag.limit) || 10;

        return await this.companyModel.aggregate([
            { $addFields: { nit: { $toString: '$nit' } } },
            {
                $match: {
                    $or: [
                        { 'name': { $regex: term, $options: 'i' } },
                        { 'address': { $regex: term, $options: 'i' } },
                        { nit: { $regex: term } },
                    ]
                }
            },
            { $sort: { createdAt: -1 } },
            { $skip: skip },
            { $limit: limit }
        ]);
    }



}
