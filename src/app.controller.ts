import { Controller, Get, Res, Req, HttpStatus, Post, Body, InternalServerErrorException } from '@nestjs/common';
import { AppService } from './app.service';
import { SeedService } from './services/seed.service';
import { Document } from "mongoose";
import { AuthService } from './auth/auth.service';
import { user, login } from "./user/entities/user.entity";
import { User } from './user/interfaces/user.interface';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';


@ApiTags('Helpers')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService, 
              private seedService: SeedService,
              private authService: AuthService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @ApiOperation({ summary: 'limpia la DB y la llena con datos nuevos seed y llena la base de datos'})
  @ApiResponse({ status: 200, description: 'retorna un objeto con el usuario adminisrador ', type: String })
  @ApiQuery({ name: 'companies', type: Number, required: false })
  @ApiQuery({ name: 'users', type: Number, required: false })
  @Get('/seed')
  async seeder(@Res() res, @Req() req):Promise<User>{
    try {
      const response = await this.seedService.createSeed({companies: req?.query?.companies || null, users: req?.query?.users || null})
      return res.status(HttpStatus.OK).json({
        message: 'Data imported succesfuly',
        adminUser: response
      })
    } catch (error) {
      throw new InternalServerErrorException(error?.message || 'Error seeding data')
    }
  } 

  @ApiOperation({ summary: 'Obtiene companies por un termino name, nit o address'  })
  @ApiResponse({ status: 200, description: 'Responde un array de objetos de company', type: user })
  @Post('auth/login')
  async login(@Res() res: any, @Body() userLogin: login): Promise<User> {
    const result = await this.authService.login(userLogin);
    if(result) return res.status(HttpStatus.OK).json({...result})
    else res.status(HttpStatus.OK).json({ message: "Usuario y/o contraseña incorrectos" })
  }

  // @Get('/mail')
  // async sendMail(@Res() res: any){
  //   try {
  //     const result = await this.mailService.sendEmail('wellcome', 'correo de bienvenida', { user: 'carlos' }, ['jego031@gmail.com', 'peterb.alfaro@gmail.com'])
  //     res.status(HttpStatus.OK).json({success: true, result })
  //   } catch (error) {
  //     res.status(HttpStatus.OK).json({ error })
      
  //   }
  // }
}



