import { Module } from '@nestjs/common';
import { CompanyController } from './company.controller';
import { ServicesModule } from '../services/services.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanyModel } from './models/company.model';
import { CompanyService } from '../services/company.service';
import { UserService } from '../services/user.service';
import { UserModel } from '../user/models/user.model';

@Module({
  imports:[
    ServicesModule,
    MongooseModule.forFeature([{name: 'Company', schema: CompanyModel}, {name: 'User', schema: UserModel} ])
  ],
  controllers: [CompanyController],
  providers: [CompanyService, UserService]
})
export class CompanyModule {}
