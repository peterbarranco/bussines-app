import { ApiProperty } from '@nestjs/swagger';
export class company {
    @ApiProperty({ example: '5fbb6dc38a152a59cf8ff649', description: 'ObjectId empresa' })
    _id:    string;
    @ApiProperty({ example: 'Google', description: 'Nombre de la compania' })
    name:    string;
    @ApiProperty({ example: '12434535', description: 'Nit de la empresa' })
    nit:     number;
    @ApiProperty({ example: 'avenida siempre viva 123', description: 'Direccion de la empresa' })
    address: string;
}
