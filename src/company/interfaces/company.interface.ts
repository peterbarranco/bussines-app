import { Document } from "mongoose";

export interface Company extends Document {
    name:    string;
    nit:     number;
    address: string;
}