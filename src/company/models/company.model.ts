import {Schema } from 'mongoose'

export const CompanyModel = new Schema({
    name:     {type: String, required: true},
    nit:      {type: Number, required: true, unique: true},
    address:  {type: String, required: true},
}, { timestamps: true, versionKey:false })