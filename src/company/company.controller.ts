import { Controller, Get, Post, Put, Delete, Res, Req, HttpStatus, Body, BadRequestException, Param, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags,
} from '@nestjs/swagger';
import { createCompanyDTO, editCompanyDTO } from './dto/Company.dto';
import { CompanyService } from '../services/company.service';
import { UserService } from '../services/user.service';
import { company } from './interfaces/company.entity';
import { Types } from "mongoose";
import { Company } from './interfaces/company.interface';


@ApiBearerAuth()
@ApiTags('Company')
@Controller('company')
export class CompanyController {

    constructor(private companyService: CompanyService, private userService: UserService){}

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Listar todas compañias'  })
    @ApiResponse({ status: 200, description: 'Responde un array de company', type: [company] })
    @Get('/')
    async getCompanies(@Res() res: any): Promise<Company>{
        const users = await this.companyService.getCompanies(); 
        return res.status(HttpStatus.OK).json({
            ok: true,
            data: users
        })
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene una company por Id'  })
    @ApiResponse({ status: 200, description: 'Responde un objeto company', type: company })
    @Get('/:companyId')
    async get(@Res() res: any, @Param('companyId') companyId: string ){
        try {
            const company = await this.companyService.get(companyId); 
            return res.status(HttpStatus.OK).json({company})
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene una company por userId'  })
    @ApiResponse({ status: 200, description: 'Responde un objeto de company', type: company })
    @Get('/byUserId/:id')
    async getByUser(@Res() res: any, @Param('id') userId: string ): Promise<Company>{
        const exists = await this.userService.get(userId);
        if(!exists) throw new BadRequestException('User not exists')
        try {
            const result = await this.companyService.get(exists.companyId.toString())
            return res.status(HttpStatus.OK).json({ result })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene una company por user email'  })
    @ApiResponse({ status: 200, description: 'Responde un objeto de company', type: company })
    @Get('/byUserEmail/:email')
    async getByUserEmail(@Res() res: any, @Param('email') userEmail: string ): Promise<Company>{
        const exists = await this.userService.getByemail(userEmail);
        if(!exists) throw new BadRequestException('User email not exists')
        try {
            const user = await this.companyService.get(exists.companyId.toString()); 
            return res.status(HttpStatus.OK).json({user})
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Obtiene companies por un termino name, nit o address'  })
    @ApiResponse({ status: 200, description: 'Responde un array de objetos de company', type: [company] })
    @Get('/byTerm/:term')
    async getByName(@Res() res: any, @Req() req, @Param('term') term: string ): Promise<Company[]>{
        const companies = await this.companyService.byTerm(term,{skip: req?.query?.skip || null, limit:  req?.query?.limit || null})
        return res.status(HttpStatus.OK).json({ companies })
    }


    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Registro de company'  })
    @ApiResponse({ status: 200, description: 'Responde un objeto de company', type: company })
    @Post()
    async create(@Res() res: any, @Body() createCompanyDTO: createCompanyDTO): Promise<Company>{
        const exists = await this.companyService.getByNit(createCompanyDTO.nit)
        if(exists) throw new BadRequestException('Company already exists')
        try {
            const company = await this.companyService.create(createCompanyDTO)
            if(company){
                return res.status(HttpStatus.OK).json({
                    ok: true,
                    data: company,
                    message:'User created successfully' 
                })
            }
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Actualiza una company'  })
    @ApiResponse({ status: 200, description: 'Responde un objeto de company', type: company })
    @Put('/:companyId')
    async edit(@Res() res: any, @Body() payload: editCompanyDTO, @Param('companyId') companyId: string ): Promise<Company>{
        const company = await this.companyService.get(companyId)
        if(!company) throw new BadRequestException('Company not exists')
        try {
            payload._id = companyId 
            const result = await this.companyService.edit(payload)
            return res.status(HttpStatus.OK).json({ ok: true, data: result })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

    @UseGuards(JwtAuthGuard)
    @ApiOperation({ summary: 'Elimina una company'  })
    @ApiResponse({ status: 200, description: 'Responde un true si de elimino o false en caso contrario', type: Boolean })
    @Delete('/:companyId')
    async delete(@Res() res: any, @Param('companyId') companyId: string ){
        try {
            const result = await this.companyService.deleteOne(companyId)
            return res.status(HttpStatus.OK).json({ ok: true, message: 'Company deleted successfully' })
        } catch (error) {
            throw new BadRequestException(error?.message || '');  
        }   
    }

    

    
}
